#wykorzystanie https://github.com/danielfrg/word2vec
#na podstawie http://nbviewer.jupyter.org/github/danielfrg/word2vec/blob/master/examples/word2vec.ipynb
import word2vec
import os
import sys

logsFilePath = './src/concat_logs_spaces'

if len(sys.argv) != 3:
  sys.exit("Podaj dwa argumenty - N (wymiar przestrzeni) i C (liczbę klastrów)")


N = sys.argv[1]
print("Wymiar przestrzeni:", N)
word2vec.word2vec(logsFilePath, './src/log.bin', size=N, verbose=False)

C = sys.argv[2]
print("Tworzę", C, "klastrów")
word2vec.word2clusters(logsFilePath, './src/clusters.txt', C, verbose=False)

model = word2vec.load('./src/log.bin')
#print(model.vocab)
clusters = word2vec.load_clusters('./src/clusters.txt')

# zapisywanie wyników do plików

directory = './wyniki/wyniki-N' + N + '-C' + C + '-alldata'
if not os.path.exists(directory):
    os.makedirs(directory)

# słownik
result = open(directory + '/result_vocab', 'w')
print(model.vocab, end="\n", file=result)
result.close()


result = open(directory + '/result_vectors_readable', 'w')
for word in model.vocab:
  print(word, end="\n", file=result)
  print(model.get_vector(word), end="\n\n", file=result)
result.close()

result = open(directory + '/result_cosine_similarity', 'w')

for word in model.vocab:
  print(word, end="\n", file=result)
  indexes, metrics = model.cosine(word)
  print(model.generate_response(indexes, metrics).tolist(), end="\n\n", file=result)

result.close()

result = open(directory + '/result_clusters', 'w')
for c in range(max(clusters.clusters) + 1):
  print(c, end=":\n", file=result)
  for card in clusters.get_words_on_cluster(c):
    print(card.decode("utf-8"), end="\n", file=result)
  print("", end="\n", file=result)
result.close()


# wektory w trzech formatach
result = open(directory + '/result_vectors', 'w')
for word in model.vocab:
  print(word, end=" ", file=result)
  for x in model.get_vector(word):
    print(x, end=" ", file=result)
  print("", end="\n", file=result)
result.close()
