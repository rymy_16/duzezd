
createCardSequences = function(no) {
  file = paste("log", no, ".log", sep = "")
  json_file = paste("../data/training_games_jsons/", file, sep = "")
  rawGameLog = fromJSON(file=json_file)
  game = rawGameLog$game

  sequence = getCardSequence(game)

  fileName = paste("../data/cardseq/log", no, ".txt", sep = "")
  file.create(fileName)
  file = file(fileName)
  writeLines(sequence, file)
  close(file)
}

getCardSequence = function(game) {
  movesNames = lapply(game, getMoveName)
  movesNames = unlist(lapply(movesNames, unname))
  movesNames = movesNames[startsWith(movesNames, "Play: ")]
  unlist(lapply(movesNames, extractCardName))
}

getMoveName = function(move) {
  move[[2]]
}

extractCardName = function(name) {
  substr(name[[1]], 18, nchar(name[[1]]))
}
