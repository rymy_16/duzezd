install.packages("rjson")
install.packages("gdata")
library("rjson")
library("gdata")

source('helperFunctions.r')

fileNumVector = 100001:399680

lapply(fileNumVector, createCardSequences)
