#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

library(cluster)
library(proxy)

if (length(args)!=2) {
  stop("Podaj dwa argumenty - N (wymiar przestrzeni) i C (liczbę klastrów)", call.=FALSE)
}
n = args[1]
c = args[2]
print(paste("N: ", n, " C: ", c))


table = read.table(
  paste("./wyniki/wyniki-N", n, "-C", c, "-alldata/result_vectors", sep = "")
)
head(table)

dataFrame = as.data.frame(table[, 2:n+1])

system.time({
  pamClustering = pam(dataFrame, k = c, metric = "euclidean");
  clusters = list()
  lapply(1:c, function(no) {
    idx = which(pamClustering$clustering == no)
    clusters[[no]] <<- table[,1][idx]
  })
})

clusters = lapply(clusters, as.character)
clusters = lapply(clusters, unlist)
clusters = lapply(clusters, paste)

outFileName = paste("./klastry/", c, "-clusters-", n, "dims.txt", sep = "")
lapply(clusters, write, outFileName, append=TRUE, ncolumns=1000)
