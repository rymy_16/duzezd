#skrypt do zajec z przedmiotu Data Mining 2017/2018: lab6 - text mining w R
#autor: Andrzej Janusz
#email: janusza@mimuw.edu.pl

#' 1. Bag-of-words, reprezentacja wektorowa tekstu, "tf-idf" - rozwiazania z pakietu tm.
#' 2. LSA.
#' 3. Grupowanie tekstow.

#' wczytywanie tekstu
library(tm)   #wymaga dodatkowej instalacji pakietu "SnowballC"
#' install.packages(c('tm', 'SnowballC'))
docsDir = "docs"

?Corpus
myCorpus = VCorpus(DirSource(file.path(getwd(), docsDir), encoding = "UTF-8"), 
                   readerControl = list(reader = readPlain, language = "en"))

length(myCorpus)
content(myCorpus)[[83]]

as.character(content(myCorpus)[[83]])
words(myCorpus[[83]])
is(words(myCorpus[[83]]))

?sents
?paras

getTransformations()

#correctEnc = function(x) {
#  stringr::str_replace_all(x,"[^[:graph:]]", " ")
#}

#myCorpus = tm_map(myCorpus, content_transformer(correctEnc))

myCorpus = tm_map(myCorpus, content_transformer(tolower))
myCorpus = tm_map(myCorpus, removeNumbers)
myCorpus = tm_map(myCorpus, removePunctuation)
myCorpus = tm_map(myCorpus, removeWords, stopwords())
myCorpus = tm_map(myCorpus, stemDocument)
myCorpus = tm_map(myCorpus, stripWhitespace)


as.character(content(myCorpus)[[83]])

?tm_filter
?tm_index

?tm_reduce

DTM = DocumentTermMatrix(myCorpus, 
                         control = list(bounds = list(global = c(2, length(myCorpus))),
                                        weighting = weightTfIdf))

is(DTM)
dim(DTM)

inspect(DTM[1:5,1:10])
inspect(DTM[1:100, "best"])
inspect(DTM["997709.txt", 1:100])

object.size(DTM)

TDM = TermDocumentMatrix(myCorpus, 
                         control = list(bounds = list(global = c(2, length(myCorpus))),
                                        weighting = weightTfIdf))

dim(TDM)
inspect(TDM[1:100, "997709.txt"])
inspect(TDM["best", 1:100])

#' Klasyfikacja danych tekstowych:
clsVec = readLines("doc_classes.csv")
library(RTextTools)
library(caTools)
trIdx = sort(sample(nrow(DTM), 1000))
container = create_container(DTM, labels = factor(clsVec), 
                             trainSize = trIdx, testSize = (1:nrow(DTM))[-trIdx],
                             virgin = FALSE)

print_algorithms()

SVMmodel = train_model(container, algorithm = "SVM",
                       cost = 10, kernel = "linear")
predictions = classify_model(container, SVMmodel)

colnames(predictions)
table(predictions$SVM_LABEL)
colAUC(predictions$SVM_PROB, clsVec[-trIdx])


#' LSA (lub inaczej LSI)
#' Technika LSA wykorzystuje rozklad SVD macierzy reprezentujacej korpus dokumentow.
system.time({
  SVD = svd(DTM)
})

#' Nowe wymiary macierzy traktujemy jako reprezentacje 'pojec' ukrytych w danych.
#' Istotnosc pojec w danym korpusie tekstow odpowiada wartosciom wlasnym.
plot(SVD$d)

#' Macierz V opisuje zwiazek pojec z kolumnami macierzy DTM, tj. z terms-ami.
dim(SVD$v)

#' Przyjrzyjmy sie reprezentacji pierwszego pojecia:
concept1 = SVD$v[,1]
summary(concept1)
concept1 = -concept1
names(concept1) = Terms(DTM)

concept1 = sort(concept1, decreasing = TRUE)
concept1[1:10]

#' Sprawdzmy dwa kolejne pojecia:
concept2 = SVD$v[,2]
concept2 = -concept2
names(concept2) = Terms(DTM)

concept2 = sort(concept2, decreasing = TRUE)
concept2[1:10]
tail(concept2)

#' Pojecia 1 i 2 sa w pewnym sensie komplementarne:
round(SVD$v[,1] %*% SVD$v[,2], 10)

concept10 = SVD$v[,10]
concept10 = -concept10
names(concept10) = Terms(DTM)

concept10 = sort(concept10, decreasing = TRUE)
concept10[1:10]

#' Stworzmy sobie nazwy pojec (bardzo naiwnie):
takeNames = function(x, xNames, n = 5) {
  names(x) = xNames
  x = sort(x, decreasing = TRUE)
  return(paste(names(x)[1:n], collapse = "_"))
}

conceptNames = apply(SVD$v, 2, takeNames, Terms(DTM), n = 6)
head(conceptNames)

#' Analiza dokumentow w przestrzeni ukrytych pojec:
conceptData = as.matrix(DTM) %*% SVD$v[,1:100]
colnames(conceptData) = conceptNames[1:100]
is(conceptData)
dim(conceptData)
object.size(conceptData)

library(slam)
conceptData = as.simple_triplet_matrix(conceptData)
matrix(conceptData[1,1:10])

#' Sprobujmy jeszcze raz wykonac klasyfikacje:
SVMmodel = e1071::svm(conceptData[trIdx, ], factor(clsVec[trIdx]), 
                      cost = 100, kernel = "linear", probability = TRUE)
predictions = predict(SVMmodel, conceptData[-trIdx, ], probability = TRUE)
table(predictions)
colAUC(attr(predictions, "probabilities")[, "class1"], clsVec[-trIdx])

#' Bardziej efektywna metoda rozkladu svd: pakiet irlba
#' install.packages("irlba")
library(irlba)

DTM2 = Matrix::Matrix(as.matrix(DTM))

system.time({
  SVD_alt = irlba(DTM2, nu = 100, nv = 100)
})

plot(SVD_alt$d)

SVD$v[1:10,2]
SVD_alt$v[1:10,2]

all(round(abs(SVD$v[, 1:10]), 10) == round(abs(SVD_alt$v[, 1:10]), 10))

#' grupowanie tekstow

#' Pogrupujmy dokumenty uzywajac klasycznego algorytmu k-srednich
system.time({
  docsClusters1 = kmeans(DTM, centers = 3, iter.max = 50, nstart = 10) 
  plot(as.matrix(conceptData[,1:2]), 
       col = docsClusters1$cluster + 1, 
       pch = docsClusters1$cluster,
       main="kmeans - podzial na 3 grupy")
})

#' Teksty warto grupowac ze wzgledu na odleglosc kosinusowa...
library(slam)
library(skmeans)
DTM = as.simple_triplet_matrix(DTM)
system.time({
  docsClusters2 = skmeans(DTM, k = 3, method = "pclust",
                          control = list(nruns = 10, reltol = 0.005)) 
  plot(as.matrix(conceptData[,1:2]), 
       col = docsClusters2$cluster + 1, 
       pch = docsClusters2$cluster,
       main="sferyczny kmeans - podzial na 3 grupy")
})

#' Mozemy rowniez wykonac grupowanie w przestrzeniu 'ukrytych pojec'... prosze poeksperymentowac
rm(list = ls())

