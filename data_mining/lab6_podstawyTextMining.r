#skrypt do zajec z przedmiotu Data Mining 2017/2018: lab6 - podstawy text mining w R
#autor: Andrzej Janusz
#email: janusza@mimuw.edu.pl

#' 1. Basic operations on character strings.
#' 2. Tokenization, stemming, POS.
#' 3. Bag-of-words, vector reprezentations of texts, "tf-idf".
#' 4. Example: document clustering.

#' reading texts
?readLines
?scan

docsDir = "docs"

docsNames = dir(file.path(getwd(), docsDir))
length(docsNames)
is.unsorted(docsNames)

docsList = list()
for(i in 1:length(docsNames)) docsList[[i]] = scan(file.path(docsDir, docsNames[i]), quote = "'",
                                                   what = character(), quiet = TRUE, allowEscapes = TRUE)
#' if we know the encoding, we can set it using "fileEncoding"

docsList[[83]]
#' concatenation of texts
paste(docsList[[83]][1], docsList[[83]][2], docsList[[83]][3], sep = " ")
paste(docsList[[83]], collapse = " ")

#' regular expressions
?regexpr
sentence1 = paste(docsList[[83]][1:grep("[.]", docsList[[83]])[1]], collapse = " ")
sentence1

sentence1 = sub(":", " - ", sentence1)
sentence1 = sub("a\\>", " ", sentence1)
sentence1 = sub("[.]", " ", sentence1)
sentence1

sentence = gsub("[[:space:]]+", " ", sentence1)
sentence = sub("[[:space:]]$", "", sentence)
sentence

#' text splitting
words = strsplit(sentence, "[[:space:]]")
words = words[[1]]                           #' the result is always a list!
words

#' Exercise:
#' divide the words from 'words' into vectors of single characters
#' and then, concatenate them into vectors of 2-shingles 
#' (only for words with 2+ characters)
#' e.g. tries -> t r i e s -> tr ri ie es
#' 

#' counting letters
sapply(words, length)
sapply(words, nchar)

words = tolower(words)
words

#' tokenization
#' install.packages(c("openNLP", "String"))
library(openNLP)
library(NLP)
myText = as.String(docsList[[83]])

myText[1,15]
myText[1,20]

annotatedText = annotate(myText,
                         list(Maxent_Sent_Token_Annotator(),
                              Maxent_Word_Token_Annotator(),
                              Maxent_POS_Tag_Annotator()))
annotatedText
nchar(paste(myText, collapse = " "))

sentences = annotate(myText, Maxent_Sent_Token_Annotator())
sentences

annotate(myText, Maxent_Word_Token_Annotator())   # error - it is necessary to annotate sentences


#' stemming
#' a stem is a 'core' of a word
library(RTextTools)

words
wordStem(words)
#' for some languages (e.g. Polish) we can find lemmas of words (their basic forms).

#' frequent words (i.e. stop-words)
library(tm)

stopwords()                            #' a list of the most common English words

length(words)
sum(words %in% stopwords())
words[words %in% stopwords()]

#' a typical text processing chain
#' For each document, we divide it into words, remove punctation, digits and duplicated white characters,
#' we also remove stop-words and words with less than 3 or more than 20 characters. 
#' In the end we do the stemming.
stemmedDocsList = lapply(docsList, function(x) gsub("[[:punct:][:digit:][:space:]]+", " ", x))
stemmedDocsList = lapply(stemmedDocsList, function(x) unlist(strsplit(x, "[[:space:]]")))
stemmedDocsList = lapply(stemmedDocsList, function(x) gsub("[[:space:]]+", "", x))
stemmedDocsList = lapply(stemmedDocsList, tolower)
stemmedDocsList = lapply(stemmedDocsList, function(x) {idx = which(nchar(x) < 3 | nchar(x) > 20 | is.na(nchar(x))); 
                                                       if(length(idx) > 0) x <- x[-idx];
                                                       x})
stemmedDocsList = lapply(stemmedDocsList, function(x, dict) {idx = which(x %in% dict); 
                                                             if(length(idx) > 0) x[-idx]}, 
                         stopwords())
stemmedDocsList = lapply(stemmedDocsList, wordStem)

stemmedDocsList[[83]]
                                                                                                     
#' "bags-of-words":
bagOfWords = table(stemmedDocsList[[83]])
bagOfWords

#' we may want to normalize the frequencies with regard to text lengths
bagOfWords = bagOfWords/sum(bagOfWords)

#' There are many methods to quantify association between a word and a document.
#' One of the most common is called "tf-idf".
#' For a single word in a text corpora, its tf-idf is a product of the frequency and
#' logaritm of 1/{frequency of texts containing this word in the corpora}.

#' Exercise:
#' compute the tf-idf vector for our bagOfWords

termDict = unique(unlist(stemmedDocsList))
termDict = termDict[order(termDict)]

length(termDict)                # pleasee note the size of this vector

vectorRep = numeric(length(termDict))
names(vectorRep) = termDict
vectorRep[names(bagOfWords)] = bagOfWords

sum(vectorRep > 0)                                
sum(vectorRep > 0)/length(vectorRep)              

#' It is more efficient to store the data in a special format which is suited for sparse data.
#' Often in such cases, it is necessary to adjust machine learning algorithms to work with
#' the sparse data format.

#' Implementations of sparse matrices in packages: Matrix, slam, SparseM
library(slam)
?Matrix

#' we may create a custom representation
makeSparse = function(stemRep, terms)  {
  numVec = table(stemRep)
  numVec = numVec/sum(numVec)
  idxVec = which(terms %in% names(numVec))
  attr(numVec, "idx") = idxVec
  return(numVec)
}

vectorDocsList = lapply(stemmedDocsList, function(x,terms) {tmp = table(x);
                                                            tmp = tmp/sum(tmp);
                                                            vec = numeric(length(terms));
                                                            names(vec) = terms
                                                            vec[names(tmp)] = tmp;
                                                            vec}, termDict)
sparseDocsList = lapply(stemmedDocsList, makeSparse, termDict)

sparseDocsList[[1]]
attr(sparseDocsList[[1]], "idx")

object.size(stemmedDocsList)
object.size(vectorDocsList)
object.size(sparseDocsList)

entityVec = unlist(mapply(function(x, y) rep(x, length(y)), 
                          1:length(sparseDocsList), sparseDocsList,
                          SIMPLIFY = FALSE))
attributeVec = unlist(lapply(sparseDocsList, attr, "idx"))
valueVec = unlist(sparseDocsList)

docsMatrix = simple_triplet_matrix(entityVec, attributeVec, valueVec)
dim(docsMatrix)
object.size(docsMatrix)

#' the same for a 'dense' matrix:
denseDocsMatrix = as.matrix(docsMatrix)
dim(denseDocsMatrix)
object.size(denseDocsMatrix)

#' sparse matrix format supports the most of typical algebraic operations:
docsMatrix[1, 338:340]
as.matrix(docsMatrix[1, 338:340])

2*docsMatrix[1, 338:340] + matrix(rep(3, 3), nrow = 1)
as.matrix(2*docsMatrix[1, 338:340] + matrix(rep(3, 3), nrow = 1))

#' another representation from the package SparseM:
docsMatrix2 = as.matrix.csr(denseDocsMatrix)
dim(docsMatrix2)
object.size(docsMatrix2)

docsMatrix2[1, 338:340]
as.matrix(docsMatrix2[1, 338:340])

#' clustering of textual data
#' 'regular' kmeans clustering:
system.time({
  docsClusters1 = kmeans(denseDocsMatrix, centers = 3, iter.max = 50, nstart = 10) 
})

table(docsClusters1$cluster)
docsClusters1$withinss
docsClusters1$totss

#' spherical kmeans is using the cosine similarity
library(slam)
library(skmeans)
system.time({
  docsClusters2 = skmeans(docsMatrix, k = 3, method = "pclust",
                          control = list(nruns = 10, reltol = 0.005)) 
})

rm(list = ls())

