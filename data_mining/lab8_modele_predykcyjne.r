#skrypt do zajec z przedmiotu Data Mining 2017/2018: lab8 - podstawowe modele predykcyjne
#autor: Andrzej Janusz
#email: janusza@mimuw.edu.pl

#1. Problem predykcji - przyklady.
#2. Ewaluacja modeli predykcji.
#3. Klasyfikator knn na przykladzie implementacji z biblioteki kknn.
#4. Naive Bayes.
#5. Regresja logistyczna.
#6. SVM.


#Tablica decyzyjna: dane wdbs
dataSet = read.table(file = "wdbc.data", header = FALSE, sep=',', row.names=1, na.strings="?")
colnames(dataSet) = c("diagnosis",
                      paste(c(rep("mean",10), rep("SE",10), rep("worst",10)),
                            rep(c("radius", "texture", "perimeter", "area",
                                  "smoothness", "compactness", "concavity",
                                  "concave_points", "symmetry", "fractal_dimension"),3),
                            sep="_"))
clsVec = dataSet[[1]]
dataSet = dataSet[-1]

binaryClass = as.integer(clsVec == "M")
X11()
plot(dataSet$worst_concave_points, dataSet$worst_perimeter, type="p", 
     pch = binaryClass, col = binaryClass + 2, cex = 1.5, 
     xlab = "worst_concave_points", ylab = "worst_perimeter", main = "WDBC data")


plot(dataSet[11:15],
     pch = binaryClass, col = binaryClass + 2, cex = 1.5, 
     main = "WDBC data")

#Podzielmy dane na dwa zbiory:
trainingIdx = sample(1:nrow(dataSet), round(3*(nrow(dataSet)/5)))
trainingSet = dataSet[trainingIdx,]
testSet = dataSet[-trainingIdx,]

clsTr = clsVec[trainingIdx]
clsTe = clsVec[-trainingIdx]

library(class)
#klasyfikator k-NN:
predictions = knn(trainingSet, testSet, clsTr, k = 3)
is(predictions)

#Jak sprawdzic czy nasze predykcje sa dobre?
#Confusion matrix:
table(clsTe, predictions)

#Accuracy:
mean(as.character(predictions) == as.character(clsTe))

#Balanced accuracy (srednie accuracy w poszczegolnych klasach decyzyjnych):
confTable = table(clsTe, predictions)
mean(diag(confTable)/apply(confTable, 1, sum))

#AUC (pole powierzchni pod krzywa ROC):
#Krzywa ROC to wykres zaleznosci miedzy "false positive rate" a "true positive rate".
#"True positive rate" to P(preds == + | trueCls == +), czyli w przyblizeniu TP/P.
#"False positive rate" to P(preds == + | trueCls == -), czyli w przyblizeniu FP/N .

#Tutaj przyda sie policzyc z jaka pewnoscia nasz klasyfikator przypisuje klase "M".

predVec = knn(trainingSet, testSet, clsTr, k = 39, prob = T)
print(predVec)

predScore = numeric(length(predVec))
predScore[predVec == "M"] = attr(predVec, "prob")[predVec == "M"]
predScore[predVec == "B"] = 1- attr(predVec, "prob")[predVec == "B"]

#Wykorzystujac "predScore" mozemy wygenerowac wiele klasyfikacji, np. preds = as.integer(predScore > 0.5)

#Gotowe rozwiazania w R
#install.packages("ROCR")
library(ROCR)
?prediction
?performance

preds = prediction(predScore, clsTe)
plot(performance(preds, measure="tpr", x.measure="fpr"), colorize=T)
slot(performance(preds, measure="auc"), "y.values")

#Aby dokladnie oszacowac jakosc klasyfikacji konieczne jest zazwyczaj wielokrotne powtorzenie procesu "trenuj/testuj"
rm(preds, predScore, predVec, predictions, confTable)

#Innym podejsciem jest 'weryfikacja krzyzowa' - zbior danych dzielimy na k rozlacznych podzbiorow, trenujemy model na
# k - 1 zbiorach i testujemy na k-tym. Czynnosc powtarzamy k razy, za kazdym biorac inny zbior do testu.
#Aby dobrze oszacowac blad, cala procedure weryfikacji krzyzowej powtarza sie kilkakrotnie, z roznymi podzialami na zbiory.
#Wazna jest przy tym jest stratyfikacja probek testowych.

#library(caret)
?caret::createDataPartition
?caret::createFolds
?caret::createMultiFolds

myFolds = caret::createFolds(clsVec, k = 10, list = TRUE, returnTrain = FALSE)
lapply(myFolds, function(x) table(clsVec[x]))

#W R wiele implementacji modeli predykcyjnych ma 'gotowe' implementacje CV
cvPreds = knn.cv(dataSet, clsVec, k = 3)

mean(as.character(cvPreds) == as.character(clsVec))


#biblioteka kknn - bardziej elastyczna implementacja k-NN
#install.packages("kknn")
library(kknn)
?kknn
?train.kknn

knnPreds = kknn(decision~., cbind(decision = as.factor(clsTr), trainingSet), testSet,
                k = 29, distance = 2,
                kernel = "biweight")$fitt

cat("Accuracy on the test set:\t", mean(knnPreds == clsTe), "\n", sep = "")
table(clsTe, knnPreds)

#inne metody przydatne do tworzenia wlasnych implementacji k-NN:
#install.packages("proxy")
library(proxy)
?proxy::dist
summary(pr_DB)
?pr_DB


#Klasyfikator Naive Bayes:
#Twierdzenie Bayesa (w najprostszej postaci): P(Y|X) = P(Y)*P(X|Y) / P(X)
#U nas X to wielowymiarowa zmienna losowa. Pojedynczy obiekt w danych traktujemy jak jej realizacje,
#czyli pojedynczy obiekt to wektor konkretnych wartosci <x1, ..., xN>. Zalozmy, ze kazda z wartosci 
#x1, ..., xN ma niezerowe prawdopodobienstwo wystapienia (czyli w uproszczeniu atrybuty maja wartosci symboliczne).
#Y to u nas decyzja (takze zmienna losowa).
#Przepisujemy twierdzenia Bayesa:
#P(Y = y|X = <x1, ..., xN>) = P(Y = y)*P(X = <x1, ..., xN>|Y = y) / P(X = <x1, ..., xN>)
#Teraz "naiwnie" zakladamy, ze <x1, ..., xN> to tak naprawde wektor realizacji N niezaleznych zmiennych losowych X1, ..., XN.
#P(Y = y|X = <x1, ..., xN>) = P(Y = y)*P(X1 = x1|Y = y)*...*P(XN = xN|Y = y) / P(X1 = x1)*...*P(XN = xN)
#Klasyfikator Naive Bayes przypisuje testowanemu obiektowi klase o najwyzszym prawdopodobienstwie a posteriori,
#czyli ta, dla ktorej iloczyn z licznika powyzszego ulamka jest najwiekszy.
#W przypadku, gdy atrybuty przyjmuja wartosci rzeczywisce (prawdopodobienstwo uzyskania konkretnej wartosci jest bliskie 0),
#wykorzystuje sie wartosci gestosci danego rozkladu.

#implementacje klasyfikatora Naive Bayes w R:
#np. biblioteki "e1071" oraz "klaR"
library(e1071)
?naiveBayes

library(klaR)
?NaiveBayes

#Jesli atrybuty w zbiorze danych przyjnuja wartosci numeryczne wystepuje klopot z obiczaniem prawdopodobienstw. 
#Mozna przyjac, ze atrybuty w danych pochodza z rozkladu normalnego (i sa niezalezne). Mozna takze zdyskretyzowac 
#dane dzielac wartosci na kilka przedzialow.

naiveBayesModel = naiveBayes(decision~., cbind(decision = clsTr, trainingSet))
class(naiveBayesModel)
summary(naiveBayesModel)

preds = predict(naiveBayesModel, testSet, type = "class")
mean(preds == clsTe)

scores = predict(naiveBayesModel, testSet, type = "raw")
dim(scores)
scores

preds = prediction(scores[, "M"], clsTe)
plot(performance(preds, measure="tpr", x.measure="fpr"), colorize=T)
slot(performance(preds, measure="auc"), "y.values")

# drzewa decyzyjne
library(rpart)

treeModel = rpart(decision~., cbind(decision = as.factor(clsTr), trainingSet), method = "class",
                  parms = list(split = "gini"),
                  control = rpart.control(minsplit = 10, cp = 0.01))
par(xpd=NA)
plot(treeModel)
text(treeModel, use.n = T, pretty=0)

preds = predict(treeModel, testSet, type = "class")
mean(preds == clsTe)

predsMatrix = predict(treeModel, testSet)
preds = prediction(predsMatrix[, "M"], clsTe)
plot(performance(preds, measure="tpr", x.measure="fpr"), colorize=T)
slot(performance(preds, measure="auc"), "y.values")

# na drzewach decyzyjnych bazuje wiele zaawansowanych algorytmów predykcyjnych:
# - random forest - library(randomForest)
# - wiele modeli 'boostingu', XGBoost - library(xgboost) ale tez znacznie starsze wersje jak library(GMB)
# - Extremely Randomized Trees - library(extraTrees)


#Regresja logistyczna:
#W klasycznym podejsciu jest to model statystyczny sluzacy przewidywaniu binarnej decyzji.
#Operuje sie w nim pojeciem "szansy" wyrazajacej stosunek prawdopodobienstwa klasy "1" do prawdopodobienstwa klasy "0".
#Logarytm "szansy" wyraza sie jako funkcje liniowa wartosci atrybutow warunkowych. Parametry modelu optymalizuje sie
#wykorzystujac dane treningowe, np. metoda "najwiekszej wiarygodnosci" lub metoda "schodzenia po gradiencie".

logit = function(p) log(p/(1-p))
plot(seq(0,1,0.001), logit(seq(0,1,0.001)), type = "l", lwd = 2)

?glm

glModel = glm(decision~., 
              cbind(decision = clsTr, trainingSet), 
              family = binomial(link = "logit"))
preds = predict(glModel, testSet, type = "response")
predsCls = preds
predsCls[preds > 0.5] = "M"
predsCls[preds <= 0.5] = "B"
mean(predsCls == clsTe)

preds = prediction(preds, clsTe)
plot(performance(preds, measure="tpr", x.measure="fpr"), colorize=T)
slot(performance(preds, measure="auc"), "y.values")

#SVM
#Zadanie optymalizacyjne:
#u_1, ..., u_n - obiekty treningowe,
#d_1, ..., d_n - klasyfikacja obiektow treningowych, d_i \in {-1,1},
#u_i %*% u_j - iloczyn skalarny (klasyczny) u_i i u_j,
#w %*% u - b = 0 - rownanie hiperplaszczyzny, w to wektor wag.

#chcemy, maksymalizowac odleglosc miedzy hiperplaszczyznami w %*% u - b = 1 i w %*% u - b = -1 tak, 
#zeby byly spelnione warunki:
# w %*% u_i - b >= 1 dla i takich, ze d_i = 1
# w %*% u_i - b <= -1 dla i takich, ze d_i = -1
#czyli d_i(w %*% u_i - b) >= 1 dla i = 1, ..., n

#odleglosc pomiedzy tymi dwiema hiperplaszczyznami wynosi 2/||w||, czyli chcemy minimalizowac norme wektora w.

#Dzieki wykorzystaniu mnoznikow Lagrange-a otrzymujemy rozwiazanie:
# w = \sum_{i}( alpha_i * d_i * u_i),
#gdzie alpha_i odpowiadaja maksimum z funkcji Lagrange-a:
#  L(alpha) = \sum_{i}( alpha_i ) - 1/2*\sum_{i,j}[ alpha_i * alpha_j * d_i * d_j * ( K(u_i, u_j) ) ],
#z warunkami alpha_i >= 0 dla i = 1, ..., n oraz \sum_{i}( d_i*alpha_i ) = 0.

#W powyzszym wzorze K(u_i, u_j) jest funkcja jadrowa (kernel) zdefiniowana jako K(u_i, u_j) = u_1 %*% u_2

?svm #implementacja libsvm w R

myRandomData = matrix(runif(200, -1, 1), 100)
myCls = apply(myRandomData, 1, function(x) return(as.integer(c(7,-3)%*%x + 1 > 0)))
myCls[myCls == 0] = -1

svmModel = svm(myRandomData, myCls, scale = F, type = "C-classification", kernel = "linear", 
               cost = 1000)

myData = as.data.frame(cbind(myCls,myRandomData))
svmModel = svm(myCls~., data = myData, type = "C-classification", kernel = "linear", 
               cost = 1000, scale = FALSE)

names(svmModel)
svmModel$coefs  #czyli nasze wartosci alpha_i
svmModel$rho    #czyli nasze przesuniecie 'b'
svmModel$index  #indeksy wektorow wspierajacych w naszych danych
svmModel$SV     #macierz wektorow wspierajacych

sum(svmModel$coefs)

#Dla powyzszego modelu mozemy narysowac hiperplaszczyzne oddzielajaca klasy decyzyjne.

w = apply((svmModel$SV) * as.numeric(svmModel$coefs),2,sum)

plot(x = myRandomData[,1], y = myRandomData[,2], pch = myCls, col = myCls + 2)
abline(coef = c(svmModel$rho/w[2],-w[1]/w[2]), col = 'black', lwd = 1)
abline(coef = c((svmModel$rho+1)/w[2],-w[1]/w[2]), col = 'red', lwd = 3)
abline(coef = c((svmModel$rho-1)/w[2],-w[1]/w[2]), col = 'red', lwd = 3)

#A co w sytuacji, gdy kilku obiektow nie da sie liniowo przypisac do prawidlowej klasy?
#Modyfikujemy problem optymalizacji przez wprowadzenie dodatkowych zmiennych odpowiadajacych karze za nieprawidlowa klasyfikacje.
#Chcemy teraz minimalizowac ||w|| + C*\sum_{i}( epsilon_i ), gdzie epsilon_i to kara za klasyfikacje i-tego obiektu, a C to stala.

#W 1995 roku Vapnik pokazal, ze rozwiazanie Lagrange-a tego problemu pozostaje prawie niezmienione.
#Wystarczy tylko dodac warunek:
#  alpha_i <= C dla i = 1, ..., n.

#Klasyfikacja nieliniowa:
myRandomData = matrix(runif(600, -1, 1), 300)
myCls = apply(myRandomData, 1, function(x) return(as.integer(sum(x^2) > 0.5)))
plot(x = myRandomData[,1], y = myRandomData[,2], pch = myCls, col = myCls + 2)

#Takich danych nie da sie sensownie podzielic hiperplaszczyzna

#W takim wypadku konieczna jest transformacja obiektow treningowych do przestrzeni o wiekszym wymiarze.

#W praktyce nie jest konieczne realizowanie takiej transformacji - mozemy wykorzystac tzw. "kernel trick".
#Dzieki temu mozemy w znaczacy sposob obnizyc zlozonosc obliczeniowa problemu optymalizacji.

#Cztery najczesciej wykorzystywane funkcje jadrowe to: liniowa, wielomianowa, eksponencjalna i sigmoidalna.
svmModel = svm(myCls~., as.data.frame(cbind(myRandomData,myCls)), type = "C-classification", kernel = "radial", cost = 100)
plot(svmModel, as.data.frame(cbind(myRandomData,myCls)))

rm(list = ls())

