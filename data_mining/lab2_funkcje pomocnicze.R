#funkcje pomocnicze do lab2 z Data Mining 2017/2018
#autor: Andrzej Janusz

#usuwanie z zbioru transakcji "itemSet" itemset-ow wskazanych przez "itemsToRm"
filterItems = function(itemSet, itemsToRm)  {
  if(length(itemsToRm) > 0) {
    idxToRm = which(itemSet %in% itemsToRm)
    if(length(idxToRm) > 0) itemSet = itemSet[-idxToRm]
  }
  return(itemSet)
}

#usuwanie pustych transakcji
removeEmptyTrans = function(itemSets) {
  if(length(itemSets) > 0)  {
    transToRm = which(sapply(itemSets, length) < 1)
    if(length(transToRm) > 0) itemSets = itemSets[-transToRm]
  }
  return(itemSets)
}

#tworzenie kandydatow na czeste transakcje o dlugosci N z transakcji o dlugosci N-1
createCandidates = function(freqItemSets)  {
  candidatesSet = list()
  if(length(freqItemSets) > 1) {
    tmpItems = strsplit(freqItemSets, "_")
    if(length(tmpItems[[1]]) > 1) {
      itemPrefixes = sapply(tmpItems, function(x) paste(x[1:(length(x)-1)], collapse = "_"))
      itemSurfixes = sapply(tmpItems, function(x) x[length(x)])
      i = 1
      while(i < length(tmpItems)) {
        tmpPrefix = itemPrefixes[i]
        tmpSurfix = itemSurfixes[i]
        j = i + 1
        while(j <= length(tmpItems) && tmpPrefix == itemPrefixes[j]) {
          candidatesSet[[length(candidatesSet) + 1]] = paste(freqItemSets[i], 
                                                             itemSurfixes[j], sep = "_")
          j = j + 1
        }
        i = i + 1
      }
    } else {
      for(i in 1:(length(freqItemSets)-1)) {
        tmpCandidate = freqItemSets[i]
        for(j in (i+1):length(freqItemSets)) {
          if(freqItemSets[j] != freqItemSets[i]) {
            candidatesSet[[length(candidatesSet) + 1]] = paste(freqItemSets[i], 
                                                               freqItemSets[j], sep = "_")
          }
        }
      }
    }
  }
  
  if(length(candidatesSet) > 0) {
    candidatesSet = unique(unlist(candidatesSet))
    candidatesSet = candidatesSet[order(candidatesSet)]
  }
  return(candidatesSet)
}

#wstepna eliminacja kandydatow na czeste transakcje przez "regule apriori"
#(tzn. kazdy podzbior czestej transakcji musi byc czesty)
aprioriEliminate = function(itemSet, freqItemSets)  {
  itemsVec = strsplit(itemSet, "_")[[1]]
  N = length(itemsVec)
  eliminateFlag = FALSE
  i = 1
  while(!eliminateFlag & i <= N) {
    if(!(paste(itemsVec[-i], collapse="_") %in% freqItemSets)) eliminateFlag = TRUE
    i = i + 1
  }
  return(eliminateFlag)
}
#koniec definicji funkcji
