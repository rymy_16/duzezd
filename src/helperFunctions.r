
createCardSequences = function(no) {
  file = paste("log", no, ".log", sep = "")
  json_file = paste("../data/training_games_jsons/", file, sep = "")
  rawGameLog = fromJSON(file=json_file)
  game = rawGameLog$game
  
  sequence0 = getCardSequence(game, 0)
  sequence1 = getCardSequence(game, 1)
  
  file0Name = paste("../data/cardseq/log", no, "_0.txt", sep = "")
  file.create(file0Name)
  file0 = file(file0Name)
  writeLines(sequence0, file0)
  close(file0)

  file1Name = paste("../data/cardseq/log", no, "_1.txt", sep = "")
  file.create(file1Name)
  file1 = file(file1Name)
  writeLines(sequence1, file1)
  close(file1)
}

getCardSequence = function(game, playerNo) {
  moves = game[unlist(lapply(game, filterPlayer, playerNo))]
  movesNames = lapply(moves, getMoveName)
  movesNames = unlist(lapply(movesNames, unname))
  movesNames = movesNames[startsWith(movesNames, "Play: ")]
  unlist(lapply(movesNames, extractCardName))
}

filterPlayer = function(oneMove, playerNo) {
  activePlayer = oneMove[[1]][[1]][[1]]
  activePlayer == playerNo
}

getMoveName = function(move) {
  move[[2]]
}

extractCardName = function(name) {
  substr(name[[1]], 18, nchar(name[[1]]))
}
